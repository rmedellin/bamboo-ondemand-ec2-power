class git {
  $package = $::operatingsystem ? {
    /(RedHat|CentOS|Fedora)/  => 'git',
    default => undef,
  }

  package { $package:
    ensure => latest,
  }
}
