class vncserver {
  $package = $::operatingsystem ? {
    /(RedHat|CentOS|Fedora)/  => 'tigervnc',
    default => undef,
  }

  package { $package:
    ensure => latest,
  }
}
