class basebuildsystem {
  stage { 'prepare': before => Stage['main'] }
  
  class build_group {
  	include emacs
  	include git
  	include subversion
  	include ant
  	include maven
  	include java
  }

  class util_group {
  	include xorg-x11
  	include vncserver
  	include xvfb
  }

  class db_group {
  	include postgresql
  	include mysql
  }
}