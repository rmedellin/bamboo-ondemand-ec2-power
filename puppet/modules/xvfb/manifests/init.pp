class xvfb {
  $package = $::operatingsystem ? {
    /(RedHat|CentOS|Fedora)/  => 'xorg-x11-server-Xvfb',
    default => undef,
  }

  package { $package:
    ensure => latest,
  }
}