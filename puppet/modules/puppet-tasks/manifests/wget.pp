define puppet-tasks::wget($target_dir, $creates = undef) {
  exec { "wget $name":
    command => "wget -c --directory-prefix $target_dir $name",
    creates => $creates,
    require => [ File[$target_dir]],
  }
}
