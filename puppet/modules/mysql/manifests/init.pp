class mysql {
  $package = $::operatingsystem ? {
    /(RedHat|CentOS|Fedora)/  => 'mysql-server',
    default => undef,
  }

  package { $package:
    ensure => latest,
  }
}