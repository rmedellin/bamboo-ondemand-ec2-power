class java {

  require File['/opt/java/sdk']

  case $::operatingsystem {
    /(RedHat|CentOS|Fedora)/: {
      case $::architecture {
        'x86_64': {}
        }
        default: {
          notice("Not available for this architecture: '$::architecture'")
        }
      }
    }
    default: {
      notice("Not available for this operatingsystem: '$::operatingsystem'")
    }
  }
}
