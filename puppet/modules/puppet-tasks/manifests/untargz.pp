define puppet-tasks::untargz($target_dir, $creates=undef, $refreshonly=true) {
  exec { "untargz $name":
    command     => "tar xzf $name --directory $target_dir",
    creates     => $creates,
    refreshonly => $refreshonly,
  }
}
