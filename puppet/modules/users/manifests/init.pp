class users {
  group { 'bamboo':
    ensure => 'present',
  }
  user { 'bamboo':
    ensure     => 'present',
    gid        => 'bamboo',
    comment    => 'bamboo agent',
    home       => '/home/bamboo',
    shell      => '/bin/sh',
    managehome => 'true',
    require    => Group['bamboo'],
  }
}