class directories {

  file { '/opt':
  	ensure => directory,
  }

  file { '/opt/java':
    ensure  => directory,
    require => File['/opt'],
  }

  file { '/opt/java/sdk':
    ensure  => directory,
    require => File['/opt/java'],
  }

  file { '/opt/bamboo-elastic-agent':
  	ensure  => directory,
  	require => File['/opt'],
  }

}