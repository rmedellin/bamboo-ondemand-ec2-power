class mercurial {
  $package = $::operatingsystem ? {
    /(RedHat|CentOS|Fedora)/  => 'mercurial',
    default => undef,
  }

  package { $package:
    ensure => latest,
  }
}
