class subversion {

  $package = $::operatingsystem ? {
    /(RedHat|CentOS|Fedora)/  => 'subversion',
    default => undef,
  }

  package { $package:
    ensure => latest,
  }
}
