class xorg-x11 {
  # Puppet doesn't support package groups yet
  $packages = $::operatingsystem ? {
    /(RedHat|CentOS|Fedora)/  => [
      'xorg-x11-server-Xorg',
      'tigervnc-server',
      'xorg-x11-server-Xvfb',
    ],
    default => undef,
  }

  package { $packages:
    ensure => latest,
  }
}
