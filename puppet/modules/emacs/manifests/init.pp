class emacs {

  $package = $::operatingsystem ? {
    /(RedHat|CentOS|Fedora)/  => 'emacs',
    default => undef,
  }

  package { $package:
    ensure => latest,
  }
}