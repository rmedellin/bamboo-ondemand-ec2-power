class postgresql {
  $package = $::operatingsystem ? {
    /(RedHat|CentOS|Fedora)/  => 'postgresql-server',
    default => undef,
  }

  package { $package:
    ensure => latest,
  }
}